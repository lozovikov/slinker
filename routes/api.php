<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('link', 'Api\LinkController@create');
Route::get('link/{link}', 'Api\LinkController@get')->where('link', '[A-Za-z0-9]{5}');