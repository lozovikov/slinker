<?php
use Collective\Html\FormBuilder;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/{code}', function (\App\Http\Repository\DbLinkRepository $repository) {
	try {
		return redirect($repository->getUrl());
	} catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
		abort(404);
	}
})->where('code', '[A-Za-z0-9]{5}');

Route::get('/qr-{link}.png', function (\App\Http\Models\Link $link)
{
	return \LaravelQRCode\Facades\QRCode::text($link->getUrl())->png();
})->where('link', '[A-Za-z0-9]{5}');