<?php
/**
 * Created by PhpStorm.
 * User: lozovikov.ra@gmail.com
 * Date: 29.07.2019
 * Time: 12:25
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Http\Models\Link::class, function (Faker\Generator $faker) {
	return [
		'url' => $faker->url,
	];
});