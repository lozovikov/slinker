<?php
/**
 * Created by PhpStorm.
 * User: lozovikov.ra@gmail.com
 * Date: 28.07.2019
 * Time: 18:03
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Models\Link;
use App\Http\Requests\LinkRequest;

class LinkController extends Controller
{
	public function get(Link $link)
	{
		return response()->json($link, 200);
	}

	public function create(LinkRequest $request)
	{
		$link = Link::create($request->all());
		return response()->json(['url' => $link->getUrl()], 201);
	}
}