<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 29.07.2019
 * Time: 20:03
 */

namespace App\Http\Repository;


interface LinkRepository
{
	public function getUrl() : string ;
}