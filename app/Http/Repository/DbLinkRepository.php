<?php
/**
 * Created by PhpStorm.
 * User: lozovikov.ra@gmail.com
 * Date: 29.07.2019
 * Time: 0:27
 */

namespace App\Http\Repository;

use App\Http\Models\Link;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DbLinkRepository implements LinkRepository
{
	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function getUrl() : string
	{
		if(!($link = Cache::get('link:'.$this->request->code))) {
			$link = Link::where('code', '=', $this->request->code)->first();
			if(empty($link)) throw new ModelNotFoundException();
			Cache::add('link:'.$link->code, $link, 3660);
		}
		return $link->url;
	}

}