<?php
/**
 * Created by PhpStorm.
 * User: lozovikov.ra@gmail.com
 * Date: 28.07.2019
 * Time: 18:05
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Link extends Model
{
	public $timestamps = false;
	protected $table = 'links';
	protected $fillable = ['url'];

	public function getRouteKeyName()
	{
		return 'code';
	}

	protected static function boot() {
		parent::boot();
		static::creating(function ($link) {
			$link->code = $link->generateCode();
		});
	}

	private function generateCode() {
		$code = Str::random(5);
		if(self::where('code', '=', $code)->first()){
			return $this->generateCode();
		} else {
			return $code;
		}
	}

	public function getUrl()
	{
		return url("/{$this->code}");
	}

}