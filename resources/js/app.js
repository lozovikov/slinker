require('./bootstrap');
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('button:not(.disabled).js-get-short-url').on('click', function() {
		let $button = $(this);
		$button.addClass('disabled').attr("disabled", true);
		let $form = $button.closest('form');
		let url = $("input[name=url]").val();
		$.ajax({
			url: $form.attr('action'),
			type: 'POST',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({'url': url}),
			success: function(data){
				$('.js-form-response').html('<div class="alert alert-success"></div>');
				$('div.alert').html(data.url);
				let url = new URL(data.url);
				$('div.alert').append('<img src="/qr-'+url.pathname.replace('/', '')+'.png">');
				$button.removeClass('disabled').removeAttr("disabled");
			},
			error: function (data) {
				$('.js-form-response').html('<div class="alert alert-danger"></div>');
				let error = data.responseJSON.errors ? data.responseJSON.errors.url[0] : data.responseJSON.message;
				$('div.alert').html(error);
				$button.removeClass('disabled').removeAttr("disabled");
			}
		});
		return false;
	});
});
