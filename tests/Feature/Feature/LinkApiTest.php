<?php

namespace Tests\Feature\Feature;

use App\Http\Models\Link;
use Tests\TestCase;

class LinkApiTest extends TestCase
{
	public function testsLinkAreCreatedCorrectly()
	{
		$payload = [
			'url' => 'https://google.com',
		];
		$this->json('POST', '/api/link', $payload)
			->assertStatus(201);
	}

	public function testsApiGetUrl()
	{
		$link = factory(Link::class)->create([
			'url' => 'https://google.com',
		]);
		$this->json('GET', '/api/link/' . $link->code)
			->assertStatus(200)
			->assertJson([
				'id' => $link->id,
				'url' => $link->url,
				'code' => $link->code
			]);
	}

	public function testLinkAreRedirectedCorrectly()
	{
		$link = factory(Link::class)->create([
			'url' => 'https://google.com',
		]);
		$this->get('/' . $link->code)->assertRedirect();
	}

	public function testsHostIsUp()
	{
		$this->get('/')->assertStatus(200);
	}
}
