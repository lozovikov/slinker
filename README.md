## Установка и запуск

Для запуска в docker

1. git clone https://bitbucket.org/lozovikov/slinker.git
2. cd slinker
2. sudo docker run --rm -v $(pwd):/app composer install
4. sudo docker-compose up -d
5. sudo docker-compose exec app composer update
5. sudo docker-compose exec app cp .env.example .env
6. sudo docker-compose exec app php artisan key:generate
7. sudo docker-compose exec app php artisan migrate

[http://localhost/](http://localhost/)

![picture](resources/img/Screenshot.png)

## Тесты

sudo docker-compose exec app composer test

## Сервер
- redis 5.0
- mysql 5.7.22
- nginx
- php-fpm
- php 7.2
